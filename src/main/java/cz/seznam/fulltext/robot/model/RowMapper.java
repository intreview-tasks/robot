package cz.seznam.fulltext.robot.model;

public class RowMapper {
    public static Row createRow(String inputRow) {
        final String[] parts = inputRow.split("\\s+");

        final String url = parts[0];
        final String contentType = parts[1];
        final int clicked = Integer.parseInt(parts[2]);

        return new Row(
                url,
                contentType,
                clicked
        );
    }
}
