package cz.seznam.fulltext.robot.model;

public class Row implements Comparable<Row> {
    private String url;

    // TODO: Implement enum for saving the space
    private String contentType;

    private final int clicked;

    public Row(String url, String contentType, int clicked) {
        this.url = url;
        this.contentType = contentType;
        this.clicked = clicked;
    }

    @Override
    public int compareTo(Row other) {
        return Integer.compare(other.clicked, clicked);
    }

    public int getClicked() {
        return this.clicked;
    }

    public String getUrl() {
        return this.url;
    }

    public String getContentType() {
        return contentType;
    }

    public String getFullLine() {
        return url + " " + contentType + " " + clicked;
    }
}
