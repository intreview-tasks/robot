package cz.seznam.fulltext.robot.task;

import cz.seznam.fulltext.robot.model.Row;
import cz.seznam.fulltext.robot.model.RowMapper;
import cz.seznam.fulltext.robot.processor.ExecutableProcessor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public class Consumer implements Runnable {
    private final BlockingQueue<String> inputQueue;
    private final ExecutableProcessor processor;
    private final CountDownLatch latch;

    public Consumer(BlockingQueue<String> inputQueue, ExecutableProcessor processor, CountDownLatch latch) {
        this.inputQueue = inputQueue;
        this.processor = processor;
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String line = inputQueue.take();

                if (line.equals(Producer.END_OF_INPUT)) {
                    break;
                }

                final Row row = RowMapper.createRow(line);
                processor.execute(row);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        } finally {
            latch.countDown();
        }
    }
}