package cz.seznam.fulltext.robot.task;

import cz.seznam.fulltext.robot.executor.AbstractParallelExecutor;

import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public class Producer implements Runnable {
    public final static String END_OF_INPUT = "END";

    private final BlockingQueue<String> inputQueue;
    private final CountDownLatch latch;

    public Producer(BlockingQueue<String> inputQueue, CountDownLatch latch) {
        this.inputQueue = inputQueue;
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            readInput(inputQueue);

            for (int i = 0; i < AbstractParallelExecutor.NUM_OF_CONSUMER_THREADS; i++) {
                inputQueue.put(Producer.END_OF_INPUT);
            }

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        } finally {
            latch.countDown();
        }
    }

    private static void readInput(BlockingQueue<String> inputQueue) {
        try (Scanner scanner = new Scanner(System.in)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                inputQueue.put(line);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
    }
}