package cz.seznam.fulltext.robot.executor;

import cz.seznam.fulltext.robot.processor.*;

import java.util.HashMap;


public class BaseParallelExecutor extends AbstractParallelExecutor {
    private final HashMap<String, ExecutableProcessor> processors = new HashMap<>();

    public BaseParallelExecutor() {
        // TODO: Should be created factory to not initialize all of these processors
        processors.put("Top", new TopProcessor());
        processors.put("ContentType", new ContentTypeProcessor());
    }

    public void process(String processorKey) throws InterruptedException {
        final ExecutableProcessor currentProcessor = processors.get(processorKey);

        startProducer();
        startConsumers(currentProcessor);

        latch.await();
        executorService.shutdown();

        if (currentProcessor instanceof FinalizablePrintable) {
            ((FinalizablePrintable) currentProcessor).print();
        }
    }
}
