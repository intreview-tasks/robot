package cz.seznam.fulltext.robot.executor;

import cz.seznam.fulltext.robot.processor.*;


public class GrepParallelExecutor extends AbstractParallelExecutor {
    public void process(String regex) throws InterruptedException {
        final GrepProcessor processor = new GrepProcessor(regex);

        startProducer();
        startConsumers(processor);

        latch.await();
        executorService.shutdown();
    }
}
