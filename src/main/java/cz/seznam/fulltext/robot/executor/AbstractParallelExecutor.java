package cz.seznam.fulltext.robot.executor;

import cz.seznam.fulltext.robot.processor.ExecutableProcessor;
import cz.seznam.fulltext.robot.task.Consumer;
import cz.seznam.fulltext.robot.task.Producer;

import java.util.concurrent.*;

public abstract class AbstractParallelExecutor {
    public final static int THREADS;
    public final static int NUM_OF_CONSUMER_THREADS;

    static {
        int numOfProcessors = Runtime.getRuntime().availableProcessors();

        if(numOfProcessors <= 3) {
            // TODO: What if the computer has only one CPU? I am ignoring this use case for the sake of simplicity.
            // Using two threads with one CPU may incur a performance penalty due to context switching.
            THREADS = Math.max(2, numOfProcessors);
        } else {
            THREADS = 4;
        }

        NUM_OF_CONSUMER_THREADS = THREADS - 1;
    }

    protected final ExecutorService executorService = Executors.newFixedThreadPool(AbstractParallelExecutor.THREADS);
    protected final CountDownLatch latch = new CountDownLatch(AbstractParallelExecutor.THREADS);

    protected final BlockingQueue<String> inputQueue = new LinkedBlockingQueue<>();

    protected void startProducer() {
        executorService.submit(new Producer(inputQueue, latch));
    }

    protected void startConsumers(ExecutableProcessor processor) {
        for (int i = 0; i <  NUM_OF_CONSUMER_THREADS; i++) {
            executorService.submit(new Consumer(inputQueue, processor, latch));
        }
    }
}
