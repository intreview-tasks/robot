package cz.seznam.fulltext.robot.arg.exception;

public class InvalidArgumentException extends RuntimeException {
    public InvalidArgumentException(String argument) {
        super(argument + " key is invalid.");
    }
}
