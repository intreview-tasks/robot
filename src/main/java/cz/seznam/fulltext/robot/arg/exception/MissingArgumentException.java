package cz.seznam.fulltext.robot.arg.exception;

public class MissingArgumentException extends RuntimeException {
    public MissingArgumentException(String missingArgument) {
        super(missingArgument + " is missing.");
    }
}
