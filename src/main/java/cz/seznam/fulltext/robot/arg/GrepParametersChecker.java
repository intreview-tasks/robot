package cz.seznam.fulltext.robot.arg;

import cz.seznam.fulltext.robot.arg.exception.InvalidArgumentException;
import cz.seznam.fulltext.robot.arg.validator.RegexValidator;
import cz.seznam.fulltext.robot.arg.validator.Validator;

import java.util.HashMap;

public class GrepParametersChecker implements ParametersChecker {
    private final HashMap<String, Validator> requiredParameterValidators = new HashMap<>();

    public GrepParametersChecker() {
        requiredParameterValidators.put("regex",new RegexValidator());
    }

    public void check(String[] args) {
        for(Validator validator : requiredParameterValidators.values()) {
            if(!validator.isValid(args)) {
                throw new InvalidArgumentException(validator.getKey());
            }
        }
    }

    public String getRegexVal() {
        return requiredParameterValidators.get("regex").getVal();
    }
}
