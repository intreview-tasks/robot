package cz.seznam.fulltext.robot.arg.validator;

public interface Validator {
    boolean isValid(String[] args);
    String getKey();
    String getVal();
}
