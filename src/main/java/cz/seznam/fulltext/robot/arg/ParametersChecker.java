package cz.seznam.fulltext.robot.arg;

public interface ParametersChecker {
    void check(String[] args);
}
