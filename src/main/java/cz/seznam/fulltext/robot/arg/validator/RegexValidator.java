package cz.seznam.fulltext.robot.arg.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexValidator implements Validator {
    private final String validationPattern = "^-r=.*?$";
    private String val;

    public boolean isValid(String[] args) {
        for (String arg : args) {
            Pattern pattern = Pattern.compile(validationPattern);
            Matcher matcher = pattern.matcher(arg);

            if(matcher.find() && arg.length() > 3) {
                val = arg.substring(3);
                return true;
            }
        }

        return false;
    }

    public String getKey() {
        return "Regex";
    }

    public String getVal() {
        return val;
    }
}
