package cz.seznam.fulltext.robot.arg;

import cz.seznam.fulltext.robot.arg.exception.MissingArgumentException;
import cz.seznam.fulltext.robot.processor.ContentTypeProcessor;
import cz.seznam.fulltext.robot.processor.GrepProcessor;
import cz.seznam.fulltext.robot.processor.TopProcessor;

import java.util.HashSet;

public class ProcessorArgChecker {
    static HashSet<String> processorKeys = new HashSet<>();

    static {
        // TODO: What about some container with processors and loop over them here?
        processorKeys.add(TopProcessor.KEY);
        processorKeys.add(ContentTypeProcessor.KEY);
        processorKeys.add(GrepProcessor.KEY);
    }

    public static void check(String[] args) {
        if(args[0] == null || !processorKeys.contains(args[0])) {
            throw new MissingArgumentException("Processor");
        }
    }
}
