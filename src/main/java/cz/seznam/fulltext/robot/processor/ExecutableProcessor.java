package cz.seznam.fulltext.robot.processor;

import cz.seznam.fulltext.robot.model.Row;

public interface ExecutableProcessor {
    void execute(Row row);
}
