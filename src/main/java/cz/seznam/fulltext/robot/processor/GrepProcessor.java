package cz.seznam.fulltext.robot.processor;

import cz.seznam.fulltext.robot.model.Row;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class GrepProcessor implements ExecutableProcessor {
    public static String KEY = "Grep";

    private final String regex;
    public GrepProcessor(String regex) {
        this.regex = regex;
    }

    @Override
    public void execute(Row row) {
        final String line = row.getFullLine();

        if(checkPattern(line, regex)) {
            System.out.println(line);
        }
    }

    private boolean checkPattern(String line, String inputPattern) {
        final Pattern pattern = Pattern.compile(inputPattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(line);
        return matcher.find();
    }
}
