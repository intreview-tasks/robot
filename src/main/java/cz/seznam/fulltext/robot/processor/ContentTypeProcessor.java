package cz.seznam.fulltext.robot.processor;

import cz.seznam.fulltext.robot.model.Row;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class ContentTypeProcessor implements ExecutableProcessor, FinalizablePrintable {
    public static String KEY = "ContentType";

    private ConcurrentHashMap<String, Integer> counter = new ConcurrentHashMap<>();

    @Override
    public void execute(Row row) {
        counter.compute(row.getContentType(), (key, value) -> (value == null) ? 1 : value + 1);
    }

    @Override
    public void print() {
        List<String> sortedKeys = counter.keySet().stream()
                .sorted()
                .collect(Collectors.toList());

        sortedKeys.forEach(contentType -> {
            Integer count = counter.get(contentType);
            System.out.println(contentType + ": " + count);
        });
    }
}
