package cz.seznam.fulltext.robot.processor;

public interface FinalizablePrintable {
    void print();
}
