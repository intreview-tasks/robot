package cz.seznam.fulltext.robot.processor;

import cz.seznam.fulltext.robot.model.Row;

import java.util.Comparator;
import java.util.PriorityQueue;

public class TopProcessor implements ExecutableProcessor, FinalizablePrintable {
    public static String KEY = "Top";
    private static final int DEFAULT_K_MOST_CLICKED = 10;
    private final PriorityQueue<Row> minHeap = new PriorityQueue<>(Comparator.reverseOrder());
    private final int maxCounter;

    public TopProcessor(int maxCounter) {
        this.maxCounter = maxCounter;
    }

    public TopProcessor() {
        this.maxCounter = TopProcessor.DEFAULT_K_MOST_CLICKED;
    }

    @Override
    public void execute(Row row) {
        add(row);
    }

    @Override
    public void print() {
        Row[] arr = toArray();

        for(Row row : arr) {
            System.out.println(row.getUrl() + " " + row.getClicked());
        }
    }

    private void add(Row row) {
        synchronized (minHeap) {
            if (minHeap.size() < maxCounter) {
                minHeap.add(row);
                return;
            }

            final Row lowest = minHeap.peek();

            if (lowest.getClicked() < row.getClicked()) {
                minHeap.remove();
                minHeap.add(row);
            }
        }
    }

    private Row[] toArray() {
        final Row[] sortedArr = new Row[minHeap.size()];

        int i = 0;
        while (!minHeap.isEmpty()) {
            sortedArr[sortedArr.length - 1 - i++] = minHeap.poll();
        }

        return sortedArr;
    }
}
