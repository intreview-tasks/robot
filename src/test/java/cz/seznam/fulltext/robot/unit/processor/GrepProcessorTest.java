package cz.seznam.fulltext.robot.unit.processor;

import cz.seznam.fulltext.robot.data.TestRowData;
import cz.seznam.fulltext.robot.helper.GrepOutputTestHelper;
import cz.seznam.fulltext.robot.model.Row;
import cz.seznam.fulltext.robot.processor.GrepProcessor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GrepProcessorTest {
    private Row[] testRowData;
    private GrepProcessor processor;
    private ByteArrayOutputStream outputStream;
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setup() {
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        testRowData = TestRowData.create();
    }

    @AfterEach
    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void testCheck_shouldPrintApplicationPdfRows() {
        // prepare
        processor = new GrepProcessor("application/pdf");

        // execute
        for(Row row : testRowData) {
            processor.execute(row);
        }


        // verify
        final List<Row> output =  GrepOutputTestHelper.convertOutputToArray(outputStream);

        assertEquals(8, output.size());

        for(Row row : output) {
            assertEquals(row.getContentType(),"application/pdf");
        }

        assertEquals("http://fakeUrl/1 application/pdf 10", output.get(0).getFullLine());
    }

    @Test
    public void testCheck_shouldPrintRowsWithCountBiggerThen500() {
        // prepare
        processor = new GrepProcessor("\\b([5-9]\\d{2}|[1-9]\\d{3,})\\b");

        // execute
        for(Row row : testRowData) {
            processor.execute(row);
        }


        // verify
        final List<Row> output = GrepOutputTestHelper.convertOutputToArray(outputStream);

        assertEquals(3, output.size());

        assertEquals(1000, output.get(0).getClicked());
        assertEquals(502, output.get(1).getClicked());
        assertEquals(861, output.get(2).getClicked());
    }

    @Test
    public void testCheck_shouldPrintNothing() {
        // prepare
        processor = new GrepProcessor("AAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

        // execute
        for(Row row : testRowData) {
            processor.execute(row);
        }

        // verify
        final List<Row> output =  GrepOutputTestHelper.convertOutputToArray(outputStream);

        assertEquals(0, output.size());

    }
}
