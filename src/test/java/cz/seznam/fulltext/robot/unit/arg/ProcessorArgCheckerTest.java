package cz.seznam.fulltext.robot.unit.arg;

import cz.seznam.fulltext.robot.arg.ProcessorArgChecker;
import cz.seznam.fulltext.robot.arg.exception.MissingArgumentException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


public class ProcessorArgCheckerTest {
    @ParameterizedTest
    @MethodSource("validArgsDataSource")
    public void testCheck_shouldCheckValidArgs(String[] args) {
        // execute + verify
        assertDoesNotThrow(() -> {
            ProcessorArgChecker.check(args);
        });
    }

    @ParameterizedTest
    @MethodSource("invalidArgsDataSource")
    public void testCheck_shouldThrowInvalidException(String[] args) {
        // execute + verify
        assertThrows(MissingArgumentException.class,() -> {
            ProcessorArgChecker.check(args);
        });
    }

    private static Stream<Arguments> validArgsDataSource() {
        return Stream.of(
                Arguments.of((Object) new String[]{"Grep","-r=http"}),
                Arguments.of((Object) new String[]{"Grep","-r=..."}),
                Arguments.of((Object) new String[]{"Top","-r=."}),
                Arguments.of((Object) new String[]{"Top","-r=application"}),
                Arguments.of((Object) new String[]{"ContentType","-r=pdf"}),
                Arguments.of((Object) new String[]{"ContentType","-r=\\w*"})
        );
    }

    private static Stream<Arguments> invalidArgsDataSource() {
        return Stream.of(
                Arguments.of((Object) new String[]{"echo","-r=http"}),
                Arguments.of((Object) new String[]{"","-r=."}),
                Arguments.of((Object) new String[]{"kubectl","-r=application"}),
                Arguments.of((Object) new String[]{"docker","-r=pdf"}),
                Arguments.of((Object) new String[]{"open","-r=\\w*"})
        );
    }
}
