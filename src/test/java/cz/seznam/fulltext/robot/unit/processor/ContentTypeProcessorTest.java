package cz.seznam.fulltext.robot.unit.processor;

import cz.seznam.fulltext.robot.data.TestRowData;
import cz.seznam.fulltext.robot.helper.ContentTypeOutputTestHelper;
import cz.seznam.fulltext.robot.model.Row;
import cz.seznam.fulltext.robot.processor.ContentTypeProcessor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ContentTypeProcessorTest {
    private Row[] testRowData;
    private ContentTypeProcessor processor;
    private ByteArrayOutputStream outputStream;
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setup() {
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        testRowData = TestRowData.create();
        processor = new ContentTypeProcessor();
    }

    @AfterEach
    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void testCheck_shouldExecuteAndCountAllTypes() {
        // execute
        for(Row row : testRowData) {
            processor.execute(row);
        }

        processor.print();


        // verify
        final Map<String, Integer> output =  ContentTypeOutputTestHelper.convertOutputToMap(outputStream);

        assertEquals(3, output.size());
        assertEquals(5, output.get("text/html"));
        assertEquals(8, output.get("application/pdf"));
        assertEquals(4, output.get("text/xml"));
    }

    @Test
    public void testCheck_shouldExecuteAndCountOnlyTextHtml() {
        // prepare
        processor.execute(testRowData[1]);
        processor.execute(testRowData[8]);

        // execute
        processor.print();


        // verify
        final Map<String, Integer> output =  ContentTypeOutputTestHelper.convertOutputToMap(outputStream);

        assertEquals(1, output.size());
        assertEquals(2, output.get("text/html"));
    }

    @Test
    public void testCheck_shouldExecuteAndCountNothing() {
        // execute
        processor.print();

        // verify
        final Map<String, Integer> output =  ContentTypeOutputTestHelper.convertOutputToMap(outputStream);

        assertEquals(0, output.size());
    }
}