package cz.seznam.fulltext.robot.unit.arg.validator;

import cz.seznam.fulltext.robot.arg.validator.RegexValidator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RegexValidatorTest {
    @ParameterizedTest
    @MethodSource("regexDataSource")
    public void testIsValid_shouldValidateArg(String arg, boolean expected) {
        // prepare
        RegexValidator validator = new RegexValidator();

        // execute
        boolean result = validator.isValid(new String[]{arg});

        // verify
        assertEquals(expected, result);
    }

    @ParameterizedTest
    @MethodSource("regexDataSource")
    public void testIsValid_shouldGetVal(
            String arg,
            @SuppressWarnings("unused")
            boolean expected,
            String expectedRegex
    ) {
        // prepare
        RegexValidator validator = new RegexValidator();

        // execute
        validator.isValid(new String[]{arg});

        // verify
        assertEquals(validator.getVal(), expectedRegex);
    }

    private static Stream<Arguments> regexDataSource() {
        return Stream.of(
                Arguments.of("", false, null),
                Arguments.of("-", false, null),
                Arguments.of("-das", false, null),
                Arguments.of("-r", false, null),
                Arguments.of("-r=", false, null),
                Arguments.of("-r=http",true,"http"),
                Arguments.of("-r=application/pdf", true,"application/pdf"),
                Arguments.of("-r=1234", true,"1234"),
                Arguments.of("-r=.", true,"."),
                Arguments.of("-r=.*", true,".*"),
                Arguments.of("-r=.+", true,".+"),
                Arguments.of("-r=.\\s", true,".\\s"),
                Arguments.of("-r=http.*", true,"http.*"),
                Arguments.of("-r=\\d+", true,"\\d+"),
                Arguments.of("-r=.+\\s\\d+", true,".+\\s\\d+")
        );
    }

}
