package cz.seznam.fulltext.robot.unit.arg;

import cz.seznam.fulltext.robot.arg.GrepParametersChecker;
import cz.seznam.fulltext.robot.arg.exception.InvalidArgumentException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


public class GrepParametersCheckerTest {
    @ParameterizedTest
    @MethodSource("validArgsDataSource")
    public void testCheck_shouldCheckValidArgs(String[] args) {
        // prepare
        GrepParametersChecker checker = new GrepParametersChecker();

        // execute + verify
        assertDoesNotThrow(() -> {
            checker.check(args);
        });
    }

    @ParameterizedTest
    @MethodSource("invalidArgsDataSource")
    public void testCheck_shouldThrowInvalidException(String[] args) {
        // prepare
        GrepParametersChecker checker = new GrepParametersChecker();

        // execute + verify
        assertThrows(InvalidArgumentException.class, () -> {
            checker.check(args);
        });
    }

    private static Stream<Arguments> validArgsDataSource() {
        return Stream.of(
                Arguments.of((Object) new String[]{"Grep","-r=http"}),
                Arguments.of((Object) new String[]{"Grep","-r=."}),
                Arguments.of((Object) new String[]{"Grep","-r=pdf"}),
                Arguments.of((Object) new String[]{"Grep","-r=+"})
        );
    }

    private static Stream<Arguments> invalidArgsDataSource() {
        return Stream.of(
                Arguments.of((Object) new String[]{"Grep"}),
                Arguments.of((Object) new String[]{"Grep","-"}),
                Arguments.of((Object) new String[]{"Grep","-r"}),
                Arguments.of((Object) new String[]{"Grep","-r="})
        );
    }
}
