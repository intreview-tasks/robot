package cz.seznam.fulltext.robot.unit.executor;

import cz.seznam.fulltext.robot.data.TestRawTextData;
import cz.seznam.fulltext.robot.executor.GrepParallelExecutor;
import cz.seznam.fulltext.robot.model.Row;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class GrepExecutorTest {
    private GrepParallelExecutor executor;
    private ByteArrayOutputStream outputStream;
    private final PrintStream originalOut = System.out;
    private final InputStream originalSystemIn = System.in;

    @BeforeEach
    public void setup() {
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        executor = new GrepParallelExecutor();
    }

    @AfterEach
    public void tearDown() {
        System.setOut(originalOut);
        System.setIn(originalSystemIn);
    }

    @Test
    public void testCheck_shouldPrintByApplicationPdf() throws InterruptedException {
        // prepare
        System.setIn(new ByteArrayInputStream(TestRawTextData.createAsOneString().getBytes()));

        // execute
        executor.process("application/pdf");

        // verify
        final List<Row> output =  convertOutputToArray();

        assertEquals(3, output.size());
        assertEquals(810, output.get(0).getClicked());
        assertEquals(4629, output.get(1).getClicked());
        assertEquals(1965, output.get(2).getClicked());
    }

    @ParameterizedTest
    @MethodSource("regexDataSource")
    public void testCheck_shouldPrintItemsByDataSource(String regex, int expectedRows) throws InterruptedException {
        // prepare
        System.setIn(new ByteArrayInputStream(TestRawTextData.createAsOneString().getBytes()));

        // execute
        executor.process(regex);

        // verify
        final List<Row> output =  convertOutputToArray();

        assertEquals(expectedRows, output.size());
    }

    private List<Row> convertOutputToArray() {
        final String[] output =  outputStream.toString().split("\n");

        if(output.length == 0 || output.length == 1 && Objects.equals(output[0], "")) {
            return new ArrayList<>();
        }

        final List<Row> outputArr = new ArrayList<>();

        for(int i = 0; i < output.length; i++) {
            final String[] textRow = output[i].split(" ");
            outputArr.add(new Row(textRow[0], textRow[1], Integer.parseInt(textRow[2])));
        }

        return outputArr;
    }

    private static Stream<Arguments> regexDataSource() {
        return Stream.of(
                Arguments.of("http:.*", 15),
                Arguments.of("4629", 1),
                Arguments.of("\\d+", 15),
                Arguments.of("\\s", 15),
                Arguments.of("ff", 1),
                Arguments.of("^http://[^/]+/[^/]+/[^/]+/[^/]+/[^/]+/", 8)
        );
    }
}