package cz.seznam.fulltext.robot.unit.executor;

import cz.seznam.fulltext.robot.data.TestRawTextData;
import cz.seznam.fulltext.robot.executor.BaseParallelExecutor;
import cz.seznam.fulltext.robot.helper.ContentTypeOutputTestHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ContentTypeBaseParallelExecutorTest {
    private BaseParallelExecutor executor;
    private ByteArrayOutputStream outputStream;
    private final PrintStream originalOut = System.out;
    private final InputStream originalSystemIn = System.in;

    @BeforeEach
    public void setup() {
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        executor = new BaseParallelExecutor();
    }

    @AfterEach
    public void tearDown() {
        System.setOut(originalOut);
        System.setIn(originalSystemIn);
    }

    @Test
    public void testCheck_shouldCountTypes() throws InterruptedException {
        // prepare
        System.setIn(new ByteArrayInputStream(TestRawTextData.createAsOneString().getBytes()));

        // execute
        executor.process("ContentType");

        // verify
        final Map<String, Integer> output = ContentTypeOutputTestHelper.convertOutputToMap(outputStream);

        assertEquals(9,output.get("text/html"));
        assertEquals(3,output.get("application/pdf"));
        assertEquals(3,output.get("text/xml"));
    }
}