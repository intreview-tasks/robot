package cz.seznam.fulltext.robot.unit.executor;


import cz.seznam.fulltext.robot.data.TestRawTextData;
import cz.seznam.fulltext.robot.executor.BaseParallelExecutor;
import cz.seznam.fulltext.robot.helper.TopOutputTestHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TopBaseParallelExecutorTest {
    private BaseParallelExecutor executor;
    private ByteArrayOutputStream outputStream;
    private final PrintStream originalOut = System.out;
    private final InputStream originalSystemIn = System.in;

    @BeforeEach
    public void setup() {
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        executor = new BaseParallelExecutor();
    }

    @AfterEach
    public void tearDown() {
        System.setOut(originalOut);
        System.setIn(originalSystemIn);
    }

    @Test
    public void testCheck_shouldPrint10MostClickedUrls() throws InterruptedException {
        // prepare
        System.setIn(new ByteArrayInputStream(TestRawTextData.createAsOneString().getBytes()));

        // execute
        executor.process("Top");

        // verify
        final ArrayList<TopOutputTestHelper.Item> output =  TopOutputTestHelper.convertOutputToArray(outputStream);

        assertEquals(10, output.size());

        assertEquals(4778,  output.get(0).getClicked());
        assertEquals(4629,  output.get(1).getClicked());
        assertEquals(4332,  output.get(2).getClicked());
        assertEquals(3919,  output.get(3).getClicked());
        assertEquals(3585,  output.get(4).getClicked());
        assertEquals(3225,  output.get(5).getClicked());
        assertEquals(2924,  output.get(6).getClicked());
        assertEquals(2788,  output.get(7).getClicked());
        assertEquals(2467,  output.get(8).getClicked());
        assertEquals(1965,  output.get(9).getClicked());

        assertEquals("http://skfzblqqmhpdpmfy.cz/xmelzmmtowoaxvsnyytxfe/pd", output.get(0).getUrl());
        assertEquals(
                "http://rhki.cz/cel/cumtyer/zrrvrcpeeivnsfgcr/fkcwdsigf/mgzyi/grkkueyszvugjux/qwcxugn/sj",
                output.get(4).getUrl()
        );
        assertEquals(
                "http://bbiqtulbfdztkutdhjipufep.cz/nfw/lhhic/ezcxktoiuifybiulpujma/v/ttbrhll/",
                output.get(9).getUrl()
        );
    }
}