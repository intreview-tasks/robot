package cz.seznam.fulltext.robot.unit.processor;

import cz.seznam.fulltext.robot.data.TestRowData;
import cz.seznam.fulltext.robot.helper.TopOutputTestHelper;
import cz.seznam.fulltext.robot.model.Row;
import cz.seznam.fulltext.robot.processor.TopProcessor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TopProcessorTest {
    private Row[] testRowData;
    private TopProcessor processor;
    private ByteArrayOutputStream outputStream;
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setup() {
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        testRowData = TestRowData.create();
        processor = new TopProcessor();
    }

    @AfterEach
    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void testCheck_shouldExecuteAndPrintAll10Items() {
        // execute
        for(Row row : testRowData) {
            processor.execute(row);
        }

        processor.print();


        // verify
        ArrayList<TopOutputTestHelper.Item> outputArr = TopOutputTestHelper.convertOutputToArray(outputStream);

        assertEquals(10, outputArr.size());

        assertEquals(1000, outputArr.get(0).getClicked());
        assertEquals(861, outputArr.get(1).getClicked());
        assertEquals(502, outputArr.get(2).getClicked());
        assertEquals(200, outputArr.get(3).getClicked());
        assertEquals(165, outputArr.get(4).getClicked());
        assertEquals(100, outputArr.get(5).getClicked());
        assertEquals(66, outputArr.get(6).getClicked());
        assertEquals(52, outputArr.get(7).getClicked());
        assertEquals(51, outputArr.get(8).getClicked());
        assertEquals(51, outputArr.get(9).getClicked());

        assertEquals("http://fakeUrl/3", outputArr.get(0).getUrl());
        assertEquals("http://fakeUrl/9", outputArr.get(2).getUrl());
        assertEquals("http://fakeUrl/6", outputArr.get(4).getUrl());
    }

    @Test
    public void testCheck_shouldExecuteAndPrint3Items() {
        // prepare
        processor.execute(testRowData[0]);
        processor.execute(testRowData[1]);
        processor.execute(testRowData[2]);


        // execute
        processor.print();


        // verify
        ArrayList<TopOutputTestHelper.Item> outputArr = TopOutputTestHelper.convertOutputToArray(outputStream);

        assertEquals(3, outputArr.size());

        assertEquals(1000, outputArr.get(0).getClicked());
        assertEquals(10, outputArr.get(1).getClicked());
        assertEquals(5, outputArr.get(2).getClicked());
    }

    @Test
    public void testCheck_shouldExecuteAndPrint0Items() {
        // execute
        processor.print();


        // verify
        ArrayList<TopOutputTestHelper.Item> outputArr = TopOutputTestHelper.convertOutputToArray(outputStream);

        assertEquals(0, outputArr.size());
    }
}