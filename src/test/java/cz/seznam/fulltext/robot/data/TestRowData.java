package cz.seznam.fulltext.robot.data;

import cz.seznam.fulltext.robot.model.Row;

public class TestRowData {
    public static Row[] create() {
        Row[] rows = new Row[17];

        rows[0] = new Row("http://fakeUrl/1","application/pdf", 10);
        rows[1] = new Row("http://fakeUrl/2","text/html", 5);
        rows[2] = new Row("http://fakeUrl/3","text/xml", 1000);
        rows[3] = new Row("http://fakeUrl/4","application/pdf", 50);
        rows[4] = new Row("http://fakeUrl/5","application/pdf", 25);

        rows[5] = new Row("http://fakeUrl/6","application/pdf", 165);
        rows[6] = new Row("http://fakeUrl/7","text/xml", 51);
        rows[7] = new Row("http://fakeUrl/8","application/pdf", 2);
        rows[8] = new Row("http://fakeUrl/9","text/html", 502);
        rows[9] = new Row("http://fakeUrl/10","application/pdf", 200);

        rows[10] = new Row("http://fakeUrl/11","text/html", 8);
        rows[11] = new Row("http://fakeUrl/12","application/pdf", 66);
        rows[12] = new Row("http://fakeUrl/13","text/xml", 52);
        rows[13] = new Row("http://fakeUrl/13","text/xml", 51);
        rows[14] = new Row("http://fakeUrl/14","application/pdf", 100);
        rows[15] = new Row("http://fakeUrl/15","text/html", 861);
        rows[16] = new Row("http://fakeUrl/15","text/html", 34);

        return rows;
    }
}
