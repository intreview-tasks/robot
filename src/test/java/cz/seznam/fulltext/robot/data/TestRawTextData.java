package cz.seznam.fulltext.robot.data;

import java.util.ArrayList;
import java.util.List;

public class TestRawTextData {
    public static List<String> create() {
        List<String> list = new ArrayList<>();

        // Add URLs to the ArrayList
        list.add("http://rhki.cz/cel/cumtyer/zrrvrcpeeivnsfgcr/fkcwdsigf/mgzyi/grkkueyszvugjux/qwcxugn/sj\ttext/html\t3585");
        list.add("http://hpbkcthejwyihslxtoqumdpzdcyfyhwdxrpflkpqacnoehrrphqpbioqwk.cz/dtyiwneumjrdmzxtnrip/tibyj/qzvnhgkwl\ttext/html\t4332");
        list.add("http://abuswukxodhfxgypzxzj.cz/iht/tiolmzyprrif/lqlqmqptypbzfjmtuv\tapplication/pdf\t810");
        list.add("http://wosttwfeemijekgfmmwdjmz.cz/zfw/cdemmwvkvbeqgzb/vqlb/exaxhmha/l/x/gbjatjlrcw/snwui/smbyugemeebjwfsgcibp/dbzzf\ttext/xml\t1591");
        list.add("http://uehjrxkbrhwndtm.cz/wvpc/bvkwivxrksccvepvdnf/dnaevzqggpvqlxuacrjaarc/znzeui/dzlb/iggwydd/udvnodunxgndrsch/vltfaofkoat/q/zg/vzhznltxkyj\ttext/html\t3225");
        list.add("http://pjhfqtd.cz/ldm/zsngcdyzh/on/alzwiurtewuoujhswlsrfnosjsrjfj/j\ttext/html\t2924");
        list.add("http://ffzkwbkzaljugtrmtnxmuamoulnazdlavuajoyvsnz.cz/\ttext/html\t1883");
        list.add("http://kxvklfjqyhltw.cz/nidhfbqvdnwhwnt/yfc/eqoeia/hfinfkmn/gmo/cagdmntmufzofehmsxb/nbtgzywfjcohypvcvwcmujiablmypiutolvq/ufnyvtgacla/kwekm/lmkgfges\tapplication/pdf\t4629");
        list.add("http://skfzblqqmhpdpmfy.cz/xmelzmmtowoaxvsnyytxfe/pd\ttext/html\t4778");
        list.add("http://bbiqtulbfdztkutdhjipufep.cz/nfw/lhhic/ezcxktoiuifybiulpujma/v/ttbrhll/\tapplication/pdf\t1965");
        list.add("http://omlkjopxfsxhpqjudosy.cz/bmnrqvhjweymtde/xwltxxsbaao/irifwzskcdylzt/cj/pfoplswfkqpnzs\ttext/html\t1498");
        list.add("http://bojqh.cz/xesebr/ncnzt/wqfjmo/\ttext/xml\t2467");
        list.add("http://mkoudwiefqnwwqlminkkqdreuofwvzrydu.cz/\ttext/html\t2788");
        list.add("http://tsjicuvkdzqtoekreoipyqd.cz/uh/z\ttext/html\t1553");
        list.add("http://svmeqkckuceopqedjmmazmgckgumggncviw.cz/\ttext/xml\t3919");

        return list;
    }

    public static String createAsOneString() {
        final StringBuilder builder = new StringBuilder();

        for(String s : create()) {
            builder.append(s).append("\n");
        }

        return builder.toString();
    }
}
