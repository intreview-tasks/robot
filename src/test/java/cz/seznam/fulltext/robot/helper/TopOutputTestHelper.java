package cz.seznam.fulltext.robot.helper;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Objects;

public class TopOutputTestHelper {
    public static class Item {
        private final String url;
        private final int clicked;

        public Item(String url, Integer clicked) {
            this.url = url;
            this.clicked = clicked;
        }

        public String getUrl() {
            return url;
        }

        public int getClicked() {
            return clicked;
        }
    }

    public static ArrayList<Item> convertOutputToArray(ByteArrayOutputStream outputStream) {
        final String[] output =  outputStream.toString().split("\n");

        if(output.length == 0 || output.length == 1 && Objects.equals(output[0], "")) {
            return new ArrayList<>();
        }

        final ArrayList<Item> outputArr = new ArrayList<>();

        for(int i = 0; i < output.length; i++) {
            final String[] inputRow = output[i].split(" ");
            outputArr.add(new Item(inputRow[0], Integer.parseInt(inputRow[1])));
        }

        return outputArr;
    }
}
