package cz.seznam.fulltext.robot.helper;

import cz.seznam.fulltext.robot.model.Row;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class GrepOutputTestHelper {
    public static List<Row> convertOutputToArray(ByteArrayOutputStream outputStream) {
        final String[] output =  outputStream.toString().split("\n");

        if(output.length == 0 || output.length == 1 && Objects.equals(output[0], "")) {
            return new ArrayList<>();
        }

        final List<Row> outputArr = new ArrayList<>();

        for(int i = 0; i < output.length; i++) {
            final String[] textRow = output[i].split(" ");
            outputArr.add(new Row(textRow[0], textRow[1], Integer.parseInt(textRow[2])));
        }

        return outputArr;
    }
}
