package cz.seznam.fulltext.robot.helper;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ContentTypeOutputTestHelper {
    public static Map<String, Integer> convertOutputToMap(ByteArrayOutputStream outputStream) {
        final String[] output =  outputStream.toString().split("\n");

        if(output.length == 0 || output.length == 1 && Objects.equals(output[0], "")) {
            return new HashMap<>();
        }

        final Map<String, Integer> outputMap = new HashMap<>();

        for(int i = 0; i < output.length; i++) {
            final String[] count = output[i].split(":");
            outputMap.put(count[0], Integer.parseInt(count[1].trim()));
        }

        return outputMap;
    }
}
