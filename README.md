
## Table of Contents

- [COMPILE](#compile)
- [LAUNCH](#launch)
- [LAUNCH-DOCKER](#launch-docker)
- [TEST](#tests)
- [DIAGRAM](#diagram)

## COMPILE
javac -d . ./src/main/java/**/*.java

## LAUNCH
#### Top: 
cat ./data/10000.txt | java -classpath . cz.seznam.fulltext.robot.Runner Top

#### ContentType:
cat ./data/10000.txt | java -classpath . cz.seznam.fulltext.robot.Runner ContentType

#### Grep:
cat ./data/10000.txt | java -classpath . cz.seznam.fulltext.robot.Runner Grep -r=REGEX
</br>
example: -r=http, -r=.*

## LAUNCH-DOCKER
- docker build -t seznam-interview .
- docker run -e FILE=./data/10000.txt -e PROCESSOR=[Top|ContentType] seznam-interview
- docker run -e FILE=./data/10000.txt -e PROCESSOR=Grep -e ADDITIONAL_ARGUMENTS="-r=SOME_PATTERN" seznam-interview


## TESTS
- mvn test

### Tests in docker
1) docker build -f DockerfileTest -t seznam-interview-test .
2) docker run seznam-interview-test

## DIAGRAM
### 1) Top processor diagram
![Alt text](./docs/top-diagram.png?raw=true "Top processor diagram")

### 2) ContentType processor diagram
![Alt text](./docs/content-type-diagram.png?raw=true "ContentType processor diagram")
