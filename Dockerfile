FROM registry.gitlab.com/food-delivery5161742/container-registry/eclipse-temurin:17
WORKDIR /app
COPY data ./data
COPY src ./src
RUN find ./src/main -name "*.java" > sources.txt && \
    javac -d . @sources.txt
CMD ["sh", "-c", "cat $FILE | java -classpath . cz.seznam.fulltext.robot.Runner $PROCESSOR $ADDITIONAL_ARGUMENTS"]
